export enum PlatformEnum {
  WINDOWS = 'windows',
  MACOS = 'macos',
  LINUX = 'linux'
}