import { injectable } from 'inversify';

import { BrowserProfile, IBrowserProfile } from '../models/BrowserProfile';
import { IBrowserProfileRepository } from './interfaces/IBrowserProfileRepository';

@injectable()
export class BrowserProfileRepository implements IBrowserProfileRepository {
  async findAll(): Promise<IBrowserProfile[]> {
    return [];
  }

  async findById(id: string): Promise<IBrowserProfile> {
    return new BrowserProfile({ id });
  }
}