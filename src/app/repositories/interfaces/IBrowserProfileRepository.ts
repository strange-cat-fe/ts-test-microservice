import { IBrowserProfile } from '../../models/BrowserProfile';

export interface IBrowserProfileRepository {
  findAll(): Promise<IBrowserProfile[]>;
  findById(id: string): Promise<IBrowserProfile>;
}