import { Model } from 'mongoose';

import { ICompany } from './ICompany';

export interface ICompanyModel extends Model<ICompany> {}