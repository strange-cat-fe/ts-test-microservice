import { model, Schema } from 'mongoose';

import { ICompany, ICompanyModel } from '.';

const CompanySchema: Schema<ICompany, ICompanyModel> = new Schema({
  name: String,
  settings: { type: Object, default: {} },
}, { versionKey: false, toJSON: { minimize: false } });

export const Company = model('Company', CompanySchema);