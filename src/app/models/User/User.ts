import bcrypt from 'bcrypt';
import { model, Schema } from 'mongoose';

import { IUser, IUserModel } from '.';

const UserSchema: Schema<IUser, IUserModel> = new Schema({
  company: {
    type: Schema.Types.ObjectId,
    ref: 'Company',
    required: true,
    index: true,
  },

  username: { type: String, required: true, index: { unique: true } },
  password: { type: String, required: true, select: false },

  columns: { type: [String], default: ['platform', 'mainWebsite', 'tags', 'notes', 'proxy', 'worktime', 'timer'] },
  columnsPresets: { type: [{ name: String, columns: [String] }] },
  columnsWidth: { type: Object, default: {} },
  
  settings: {
    language: { type: String, enum: [null, 'en', 'ru'], default: null },
    theme: { type: String, enum: ['dark', 'light'], default: 'dark' },
    dolphinIntegration: {
      token: { type: String, default: null },
    },
  },
}, { versionKey: false, toJSON: { minimize: false } });

UserSchema.pre('save', async function (next) {
  const user = this;

  if (!user.isModified('password')) return next();

  try {
    user.password = await bcrypt.hash(user.password, 10);
    return next;
  } catch (e) {
    return next(e);
  }
});

UserSchema.methods.comparePassword = function (candidatePassword: string, next) {
  bcrypt.compare(candidatePassword, this.password, (err, same) => {
    if (err) return next(err);

    next(null, same);
  });
};

export const User = model('User', UserSchema);