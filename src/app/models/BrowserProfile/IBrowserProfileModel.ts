import { Model } from 'mongoose';

import { IBrowserProfile } from './IBrowserProfile';

export interface IBrowserProfileModel extends Model<IBrowserProfile> {}