import { model, Schema } from 'mongoose';

import { IBrowserProfile, IBrowserProfileModel } from '.';

const BrowserProfileSchema: Schema<IBrowserProfile, IBrowserProfileModel> = new Schema(
  {
    company: {
      type: Schema.Types.ObjectId,
      ref: 'Company',
      required: true,
      index: true,
    },
    user: {
      type: Schema.Types.ObjectId,
      ref: 'User',
      required: true,
      index: true,
    },
    name: { type: String, required: true },
    platform: { type: String, required: true, enum: ['windows', 'macos', 'linux'] },
    proxy: {
      type: Schema.Types.ObjectId,
      ref: 'Proxy',
      index: true,
    },
    tags: { type: [String], default: [] },
  },

  {
    toJSON: { virtuals: true },
  },
)

BrowserProfileSchema.virtual('id').get(function (this: IBrowserProfile) { 
  return this._id.toString();
});

export const BrowserProfile = model('Browser_Profile', BrowserProfileSchema);