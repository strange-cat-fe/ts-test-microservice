import { IBrowserProfile } from '../../models/BrowserProfile';

export interface IBrowserProfileService {
  getAll(): Promise<IBrowserProfile[]>;
  getById(id: string): Promise<IBrowserProfile>;
}