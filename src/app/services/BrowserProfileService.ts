import { inject, injectable } from 'inversify';

import { Types } from '../../inversify-config/types';
import {
  IBrowserProfileRepository,
} from '../repositories/interfaces/IBrowserProfileRepository';
import { IBrowserProfileService } from './interfaces/IBrowserProfileService';

@injectable()
export class BrowserProfileService implements IBrowserProfileService {
  constructor(
    @inject(Types.BrowserProfileRepository) private _browserProfileRepository: IBrowserProfileRepository
  ) { }

  async getAll() {
    return await this._browserProfileRepository.findAll();
  }

  async getById(id: string) {
    return await this._browserProfileRepository.findById(id);
  }
}