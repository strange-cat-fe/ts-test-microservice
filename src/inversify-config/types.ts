export const Types = {
  BrowserProfileRepository: Symbol.for('BrowserProfileRepository'),
  BrowserProfileService: Symbol.for('BrowserProfileService'),
}