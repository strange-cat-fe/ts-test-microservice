import 'reflect-metadata';

import { Container } from 'inversify';

import { Types } from './types';

class BrowserProfileRepository_test {};
class BrowserProfileService_test {};

const testContainer = new Container();

// Repositories
testContainer.bind(Types.BrowserProfileRepository).to(BrowserProfileRepository_test).inSingletonScope();

// Business
testContainer.bind(Types.BrowserProfileService).to(BrowserProfileService_test).inSingletonScope();

export { testContainer };