import 'reflect-metadata';

import { Container } from 'inversify';

import { BrowserProfileRepository } from '../app/repositories/BrowserProfileRepository';
import { BrowserProfileService } from '../app/services/BrowserProfileService';
import { Types } from './types';

const prodContainer = new Container();

// Repositories
prodContainer.bind(Types.BrowserProfileRepository).to(BrowserProfileRepository).inSingletonScope();

// Services
prodContainer.bind(Types.BrowserProfileService).to(BrowserProfileService).inSingletonScope();

export { prodContainer };